#!/usr/bin/env python
# coding: utf-8

# In[1]:


# for box detaction

import os
import time
import statistics
import cv2
from scipy.stats import mode

# for image preprocess
from numpy import loadtxt
import pandas as pd
from PIL import Image,ImageFilter
import numpy as np
import sys
import csv
import PIL.ImageOps

# for model
# %tensorflow_version 1.x  # For colab
import tensorflow
from tensorflow.python import keras
from tqdm import tqdm
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from PIL import ImageTk, Image

# importing libraries 
# from tkinter import *
import tkinter as tk 
from tkinter import Message, Text 
import shutil 
from PIL import Image, ImageTk 
import datetime 
import tkinter.ttk as ttk 
import tkinter.font as font 
from pathlib import Path 


# In[2]:


model = load_model('.\\model_letters_digits_space_sc_89_r2.h5')


# ## Code for finding boxes

# ###  Preprocessing

# In[3]:


def preprocess(img,value):

  # Convert cv2 image to PIL image
  #cv2_img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
    image = Image.fromarray(img)
    image = image.crop((4, 3, image.size[0]-4, image.size[0]-4))     # im.crop((left, top, right, bottom)) 
    img_resized = image.resize((78,78))
    img_resized=PIL.ImageOps.invert(img_resized)
  # img_file = img_resized.filter(ImageFilter.MedianFilter())
    threshold = 80
    img_file = img_resized.point(lambda p: p > threshold and 255)

  # Save Greyscale values
    value = np.asarray(img_file.getdata(), dtype=np.int).reshape((img_file.size[1], img_file.size[0]))
    value = value.flatten()

  # To improve image quality on 4 top and bottom lines
    for i in range (0,4):
        if (mode(value[78*i:78*(i+1)])[0]>200 or (mode(value[78*i:78*(i+1)])[0] < 80 and mode(value[78*i:78*(i+1)])[1][0] < 54)):
              value[78*i:78*(i+1)]=0
        if (mode(value[-78*(i+1):-78*(i)-1])[0]>200 or (mode(value[-78*(i+1):-78*(i)-1])[0] < 80 and mode(value[-78*(i+1):-78*(i)-1])[1][0] < 54)):
              value[-78*(i+1):-78*(i)-1]=0
    return(value)


# In[4]:


# Image prediction

def inference(test,count,row_list):

    test_image = []
#     print('inference inp',(test,count))
  # Saivng all images in X_test
    for i in tqdm(range(test.shape[0])):
        im_buf = test.values[i][0:]
        im_array = np.int8(np.reshape(im_buf, (78, 78))) 
        #img = image.load_img(train.iloc[:,1:].as_matrix().astype('str'), target_size=(64,64,1), color_mode='grayscale')
        img = image.img_to_array(im_array)
        img = img/255
        test_image.append(img)
    X_test = np.array(test_image)
    X_test = np.repeat(X_test, 3, -1)
    
#     print("Predicting")
    op=[]
    op=infer_v2(X_test,count,row_list)
    
    #print(op)
    return((count,"".join(op)[::-1]))


# In[5]:


def infer_v2(x_test,count,row_list):
    indices={'A': 0,'B': 1,'C': 2,'D': 3,'E': 4,'F': 5,'G': 6,'H': 7,'I': 8,'J': 9,'K': 10,'L': 11,'M': 12,'N': 13,'O': 14,'P': 15,
         'Q': 16,'R': 17,'S': 18,'T': 19,'U': 20,'V': 21,'W': 22,'X': 23,'Y': 24,'Z': 25,'0': 26,'1': 27,'2': 28,'3': 29,
         '4': 30,'5': 31,'6': 32,'7': 33,'8': 34,'9': 35,' ': 36,'-': 37,'/': 38,'@': 39, ',': 40,'.': 41,':': 42,
         ')': 43,'(': 44,'&': 45,'+': 46}
    indices=list(indices.keys())
    op=[]
    n=0
    len_row=len(set(row_list))
    
    # Predicting the value
    pred=model.predict(x_test)
    #print(len(pred))
    if (count==0):
        #print(str(count)," count")
        output=[]
        
        # Traversing through all predictions
        for i in pred:
            #print(i)
            
            # Sorting the probability in decreasing order classes
            output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
            #print(output)
            
            # Traversing through each infer and checking if index follows custom rule as per row base.
            for value,index in output:
                if (n==5):
                    if (index <= 25 or index==36):
                        #op.append(list(indices.keys())[index])
                        op.append(indices[index])
                        n+=1
                        break
                    else:
                        continue

                else:
                    if (index <= 25 or index > 36):
                        continue
                    else:
                        #op.append(list(indices.keys())[index])
                        op.append(indices[index])
                        n+=1
                        break
                        
# Applying custom rule on section 2
    elif (count==1):
        #print(str(count)," count")
        output=[]
        for i in pred:
            #print(i)
            output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
            #print(output)
            for value,index in output:
                
                if (index <= 25 or index==36):
                    #print(index)
                    #op.append(list(indices.keys())[index])
                    op.append(indices[index])
                    #print(index,"here1")
                    break
                else:
                   # print(index,"here2")
                    continue
# Applying custom rule on section 3                  
    elif (count==2):
        output=[]
        if (len_row==8):
        #print(row_list)
        #print(str(count)," count")

            for i in pred:

                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (row_list[n]==0):

                        if (index <= 25 or index > 36):
                            continue
                        else:
                            #op.append(list(indices.keys())[index])
                            op.append(indices[index])
                            n+=1
                            break
                    elif (row_list[n]<=3):

                        if (index <= 25 or index == 36 or index == 37):
                            #op.append(list(indices.keys())[index])
                            op.append(indices[index])
                            n+=1
                            break
                        else:
                            continue
                    else:
                        #op.append(list(indices.keys())[index])
                        op.append(indices[index])
                        n+=1
                        break
                            
        else:
            for i in pred:

                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (n <= 5):

                        if (index <= 25 or index > 36):
                            continue
                        else:
                            #op.append(list(indices.keys())[index])
                            op.append(indices[index])
                            n+=1
                            break
                    elif (n <=68):

                        if (index <= 25 or index == 36 or index == 37):
                            #op.append(list(indices.keys())[index])
                            op.append(indices[index])
                            n+=1
                            break
                        else:
                            continue
                    else:
                        op.append(indices[index])
                        #op.append(list(indices.keys())[index])
                        n+=1
                        break

        
# Applying custom rule on section 4    
    elif (count==3):
        output=[]
        if (len_row==4):
        #print(row_list)
        #print(str(count)," count")

            for i in pred:

                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (row_list[n]==0 or row_list[n]==2):

                        if (index <= 25 or index > 36):
                            continue
                        else:
                           # op.append(list(indices.keys())[index])
                            op.append(indices[index])
                            n+=1
                            break
                    else:

                        if (index <= 25 or index == 36):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue

                            
        else:
            for i in pred:

                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (n <= 28):

                        if (index <= 25 or index > 36):
                            continue
                        else:
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                    elif (n <=57):

                        if (index <= 25 or index == 36):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue
                    elif (n <= 86):

                        if (index <= 25 or index > 36):
                            continue
                        else:
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                    elif (n <=115):

                        if (index <= 25 or index == 36):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue
                            
# Applying custom rule on section 5                          
    elif (count==4):
        
        output=[]
        if (len_row==2):
        #print(row_list)
        #print(str(count)," count")

            for i in pred:

                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (row_list[n]==0):

                        if (index <= 25 or index == 36 or index == 37 or index == 46):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue
                            
                    else:
                        if (n <= 21):
                            if (index <= 25 or index==36):
                                op.append(indices[index])
                                #op.append(list(indices.keys())[index])
                                n+=1
                                break
                            else:
                                continue
                        elif (n >= 21):
                            if (index >=26 and index <= 36):
                                op.append(indices[index])
                                #op.append(list(indices.keys())[index])
                                n+=1
                                break
                            else:
                                continue
                            
        else:
            for i in pred:

                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (n <=10):

                        if (index <= 25 or index == 36 or index == 37 or index == 46):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue
                            
                    elif (n <= 21):
                        if (index <= 25 or index==36):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue
                    else:
                        if (index >=26 and index <= 36):
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
                        else:
                            continue
                            

                            
# Applying custom rule on section 6
    elif (count==5):
        print(row_list)
        #print(str(count)," count")
        output=[]
        if (len_row==2):
            for i in pred:
                
                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (row_list[n]==0):
                        op.append(indices[index])
                        n+=1
                        #op.append(list(indices.keys())[index])
                        break 
                    else:
                        if (index <= 25 or index > 36):
                            continue
                        else:
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break
        else:            
            for i in pred:
                
                #print(i)
                output=sorted(((value, index) for index, value in enumerate(i)), reverse=True)
                #print(output)
                for value,index in output:
                    if (n<=30):
                        op.append(indices[index])
                        n+=1
                        #op.append(list(indices.keys())[index])
                        break 
                    else:
                        if (index <= 25 or index > 36):
                            continue
                        else:
                            op.append(indices[index])
                            #op.append(list(indices.keys())[index])
                            n+=1
                            break

                            
    return(op)


# In[6]:


# Load the image in black and white (0 - b/w, 1 - color).
def inner_box(img,count,height):
  # img is image
  # Count is section 0-5
  # height is image height
  # number is image number from the batch
#     print('inner box inp',(img,count,height))
  #Invert the image to be white on black for compatibility with findContours function.
    imgray = 255 - img
    row=0
    row_list=[]
# Using different combination of dilation and blur to get as much as contours possible

  # rects1
    kernel = np.ones((1,10),np.uint8)
    imgray1 = cv2.dilate(imgray,kernel,iterations = 1)
    kernel = np.ones((10,1),np.uint8)
    imgray1 = cv2.dilate(imgray1,kernel,iterations = 1)
    thresh1 = cv2.adaptiveThreshold(imgray1, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)

  #Find all the contours in thresh. 
    contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects1 = [cv2.boundingRect(cnt) for cnt in contours]


  # rects2 imp
    imgray2 = cv2.blur(imgray,(10,3))
    thresh2 = cv2.adaptiveThreshold(imgray2, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)


      #Find all the contours in thresh. 
    contours, hierarchy = cv2.findContours(thresh2, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects2 = [cv2.boundingRect(cnt) for cnt in contours]

      # rects3 imp
    imgray3 = cv2.blur(imgray,(3,10))
    thresh3 = cv2.adaptiveThreshold(imgray3, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)

      #Find all the contours in thresh. 
    contours, hierarchy = cv2.findContours(thresh3, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects3 = [cv2.boundingRect(cnt) for cnt in contours]

      #rects4 imp
    kernel = np.ones((2,7),np.uint8)
    imgray4 = cv2.dilate(imgray,kernel,iterations = 1)
    thresh4 = cv2.adaptiveThreshold(imgray4, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)

      #Find all the contours in thresh.
    contours, hierarchy = cv2.findContours(thresh4, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects4 = [cv2.boundingRect(cnt) for cnt in contours]


      #rects5 imp
    kernel = np.ones((7,2),np.uint8)
    imgray5 = cv2.dilate(imgray,kernel,iterations = 1)
    thresh5 = cv2.adaptiveThreshold(imgray5, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)

      #Find all the contours in thresh.
    contours, hierarchy = cv2.findContours(thresh5, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects5 = [cv2.boundingRect(cnt) for cnt in contours]

      #rects6 imp
    kernel = np.ones((7,7),np.uint8)
    imgray6 = cv2.dilate(imgray,kernel,iterations = 1)
    thresh6 = cv2.adaptiveThreshold(imgray6, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)

      #Find all the contours in thresh.
    contours, hierarchy = cv2.findContours(thresh6, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects6 = [cv2.boundingRect(cnt) for cnt in contours]
    
      #rects7 imp
    kernel = np.ones((10,10),np.uint8)
    imgray7 = cv2.dilate(imgray,kernel,iterations = 1)
    thresh7 = cv2.adaptiveThreshold(imgray7, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,15,2)

      #Find all the contours in thresh.
    contours, hierarchy = cv2.findContours(thresh7, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    rects7 = [cv2.boundingRect(cnt) for cnt in contours]


 # Defining function to filter sections for different size sections
    def filterrects1(raw_rects,rects):
        for i in raw_rects:
              if (abs(i[2]-i[3])<10):
                    if (i[2]>height/75 and i[2]<height/40 and i[3]>height/75 and i[3]<height/40 and i not in rects):
                        rects.append(i)
        return(rects)
  
    def filterrects2(raw_rects,rects):
        for i in raw_rects:
            if (i[2]>height/40 and i[2]<height/25 and i[3]>height/40 and i[3]<height/25 and i not in rects):
                  rects.append(i)
        return(rects)
  
 # checking the section number and calling function accordingly.
    if count!=0:
        rects=[]
        rects=filterrects1(rects1,rects)
        rects=filterrects1(rects2,rects)
        rects=filterrects1(rects3,rects)
        rects=filterrects1(rects4,rects)
        rects=filterrects1(rects5,rects)
        rects=filterrects1(rects6,rects)
        rects=filterrects1(rects7,rects)

    else:
        rects=[]
        rects=filterrects2(rects1,rects)
        rects=filterrects2(rects2,rects)
        rects=filterrects2(rects3,rects)
        rects=filterrects2(rects4,rects)
        rects=filterrects2(rects5,rects)

    rects_final=rects.copy()
  
 # Removing the sections which are overlapping
    for i in range(0,len(rects)-1):
        try:
            for j in range(i+1,len(rects)):
                if (abs(rects[i][0] - rects[j][0]) < 15 and abs(rects[i][1] - rects[j][1]) < 15):
                    if (rects[i][2] < rects[j][2] and rects[i][3] < rects[j][3]):
                        rects_final.remove(rects[j])
                    else:
                        rects_final.remove(rects[i])
        except:
            continue
    rects=rects_final.copy()

  #Calculate the combined bounding rectangle points.
    top_x = min([x for (x, y, w, h) in rects])
    top_y = min([y for (x, y, w, h) in rects])
    bottom_x = max([x+w for (x, y, w, h) in rects])
    bottom_y = max([y+h for (x, y, w, h) in rects])


    from scipy.stats import mode
    h_mode = mode([h for (x, y, w, h) in rects])[0]
  #w_mode = mode([w for (x, y, w, h) in rects])[0]
    n=int(((bottom_y-top_y)/h_mode))

# Sorting the sections in increasing order as they occur in image
    import operator
    temp=[]
    b = [[] for i in range(0, n+3)]
    for j in range (1,n+3):
        for i in rects:
            x=i[0]
            y=i[1]
            w=i[2]
            h=i[3]
          
    # Keeping constraint on y and sorting as per x pixels
            if ((y > bottom_y-h_mode*j-h_mode/2.5) and (i not in temp)):
                b[j].append(i)
                temp.append(i)
            b[j]=sorted(b[j],key=lambda x:(-x[0]))
  #b.pop(0)
#   tar_dir=r"/content/gdrive/My Drive/Colab Notebooks/Output/Inner/"+str(2)+"/"+str(count)+"/"
#   if not os.path.exists(tar_dir):
#       #print("here2")
#     os.makedirs(tar_dir) 

# Traversing through each section and saving and predicting each contour
    count1=0
#     print(b)

# dataframe to store all images values
    columnNames=[]
    for i in range (0,6084):
        columnNames.append(i)
#     print("count is",count)
    data=pd.DataFrame(columns = columnNames)
    num_images=0
    #print(b)
    
    for li in b:
        if li==[]:
            b.remove(li)
    
    for i in b:
        row+=1
        for j in i:
            try:
                x=j[0]
                y=j[1]
                w=j[2]
                h=j[3]

        #To append image in dataframe
                value=[]
        #         cv2.imwrite(tar_dir+str(count1)+".jpg",img[y-2:y+h+2,x-2:x+w+2])
                value=preprocess(img[y-2:y+h+2,x-2:x+w+2],value)
                #data,num_images=preprocess(data,img[y-2:y+h+2,x-2:x+w+2],num_images)
                data.loc[num_images]=value
                row_list.append(row)
                num_images += 1
                time.sleep(0.01)
                count1+=1
            except:
                row-=1
                continue
    row_list=row_list-np.min(row_list)
    lbl_sec=inference(data,count,row_list)
    #print(count)
    return lbl_sec


# In[7]:


# to check if all major boxes came and try to get correct one by hard code if some error.

def validrects(rects3,height):
    
    
    if len (rects3)!=6:
        rects_f=[]
        
        # saving all cooredinates of large box by manuualy calculating x,y,w,h using height of whole image and 1st box
        first=rects3[0]
        second=(first[0],first[1]+first[3]+round(height/159.41),round(first[2]*2.05),round(height/11.13))
        third=(first[0],second[1]+second[3]+round(height/159.41),round(first[2]*2.05),round(height/4.45))
        fourth=(first[0]+(round(first[2]*2.05*5/35)),third[1]+third[3]+round(height/159.41),round(first[2]*2.05),round(height/8.37))
        fifth=(first[0]+(round(first[2]*2.05*5/35)),fourth[1]+fourth[3]+round(height/159.41),round(first[2]*2.05),round(height/11.89))
        sixth=(first[0]+(round(first[2]*2.05*4/35)),fifth[1]+fifth[3]+round(height/159.41),round(first[2]*2.05),round(height/8.77))
        rects_f=[first,second,third,fourth,fifth,sixth]
        return rects_f
    
    elif (len (rects3)==6 and  rects3[1][3]/rects3[0][3] > 1.25 and rects3[2][3]/rects3[0][3] > 3.25 and 
    rects3[3][3]/rects3[0][3] > 1.50 and rects3[4][3]/rects3[0][3] > 1.25 and rects3[5][3]/rects3[0][3] > 1.50):
        return rects3
    else:
        rects_f=[]
        # saving all cooredinates of large box by manuualy calculating x,y,w,h using height of whole image and 1st box
        first=rects3[0]
        second=(first[0],first[1]+first[3]+round(height/159.41),round(first[2]*2.05),round(height/11.13))
        third=(first[0],second[1]+second[3]+round(height/159.41),round(first[2]*2.05),round(height/4.45))
        fourth=(first[0]+(round(first[2]*2.05*5/35)),third[1]+third[3]+round(height/159.41),round(first[2]*2.05),round(height/8.37))
        fifth=(first[0]+(round(first[2]*2.05*5/35)),fourth[1]+fourth[3]+round(height/159.41),round(first[2]*2.05),round(height/11.89))
        sixth=(first[0]+(round(first[2]*2.05*4/35)),fifth[1]+fifth[3]+round(height/159.41),round(first[2]*2.05),round(height/8.77))
        rects_f=[first,second,third,fourth,fifth,sixth]
        return rects_f
        return rects3
        


# In[8]:


def getdata(path):
        #Load the image in black and white (0 - b/w, 1 - color).
    # img = cv2.imread(r'C:\Users\Rahul\Desktop\Capstone\fwddocuments\8.jpg', 0)
#     img = cv2.imread(r'C:\Users\Ankit\Desktop\CAPP_OCR\CAPP_GUI\Batches\B1\1.jpg', 0)
    img = cv2.imread(path, 0)
    #Get the height and width of the image.
    height, width = img.shape[:2]

    #Invert the image to be white on black for compatibility with findContours function.
    imgray = 255 - img

    # Remove noise using dilate and thershold
    thresh = cv2.adaptiveThreshold(imgray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,15,2)
    kernel = np.ones((2,2),np.uint8)
    thresh = cv2.dilate(thresh,kernel,iterations = 1)

    #Find all the contours in thresh. In your case the 3 and the additional strike
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

    #Find bounding rectangles for each contour.
    rects = [cv2.boundingRect(cnt) for cnt in contours]
    rects2=[]
    count=0
    for i in rects:
        # hierarchy [Next, Previous, First_Child, Parent]
        if (hierarchy[0][count][2]>0 and hierarchy[0][count][3]<0):   # condn that contour is parent and have child.
            if (i[2]>thresh.shape[1]/3 and i[2] < thresh.shape[1]):   # condn on width of contours
                rects2.append(i)
        count+=1
    h_mode = mode([h for (x, y, w, h) in rects2])[0]
    #print(h_mode)
    # to remove noisy contour i.e. which are small in height if these
    if h_mode < height/20:
        rects2=[]
        count=0
        for i in rects:
            if (hierarchy[0][count][2]>0):
                if (i[2]>thresh.shape[1]/3 and i[2] < thresh.shape[1] and i[3] > h_mode*1.2 ):
                    rects2.append(i)
            count+=1

    # to remove duplicate contours having very less coordinate difference
    rects3=rects2.copy()
    for i in range(0,len(rects2)-1):
        try:
            for j in range(i+1,len(rects2)):
                if (rects2[i][1] - rects2[j][1] <50):
                    if rects2[i][2]>rects2[j][2]:
                        value=rects2[j]
                        rects3.remove(value)
                    else :
                        value=rects2[i]
                        rects3.remove(value)
        except:
            continue

    # sort in correct order
    rects3=sorted(rects3,key=lambda x:(x[1]))
    rects3=validrects(rects3,height)
    # saving all
    count=0
#     print('length of rects3',len(rects3))
#     out_rects=[]
    pred_val={}
    for j in rects3:
        #print(j)
#         try:
            x=j[0]
            y=j[1]
            w=j[2]
            h=j[3]
            lbls=inner_box(img[y:y+h,x:x+w],count,height)
#             print(lbls)
            pred_val[lbls[0]]=lbls[1]
    #         cv2.imwrite(r"C:\Users\Rahul\Desktop\Capstone\form\img"+str(count)+".jpg",img[y:y+h,x:x+w])
#             cv2.imwrite(r"C:\Users\Ankit\Desktop\CAPP_OCR\CAPP_GUI\Batches\B1\img"+str(count)+".jpg",img[y:y+h,x:x+w])
            time.sleep(0.001)
            count+=1
#         except:
#             continue
    #print(pred_val)
    return pred_val


# ## radio-GUI

# In[9]:



window = tk.Tk() #the window to be used
window.title("One Tap Updater") #title for the window
window.configure(background ='light goldenrod yellow') 
window.geometry('2000x800') #setting the window size


#The Label widget is a standard Tkinter widget used to display a text or image on the screen.
message = tk.Label( 
    window, text ="One Tap Updater",bg='light goldenrod yellow',
     fg = "black", width = 50, 
    height = 3, font = ('calibri', 30, 'bold')) #setting the attributes for the Head title
message.place(x = 200, y = 20) 

#defining atrributes for the text "Path of Scanned Images"
lbl = tk.Label(window, text = "Path of Scanned Images", 
width = 20, height = 1, fg ="black",font = ('calibri', 15, ' bold ') )  
lbl.place(x = 400, y = 215) 

#defining attributes for the box that will recieve the path
txt = tk.Entry(window, 
width = 40, bg ="white", 
fg ="black", font = ('calibri', 15, ' bold ')) 
txt.place(x = 700, y = 215) 
txt.focus() 

tkvar = tk.StringVar(window) #window variable for update options
var = tk.IntVar()  #window variables for the file chosen

#setting scrollbar
scrollbar = tk.ttk.Scrollbar(window,orient=tk.VERTICAL)
scrollbar.place(x=890,y=500)

#creating message box for displaying appropiate messages during exection 
mylist = tk.Listbox(window,width=60,height=5,
                    bg='white',font = ('calibri', 12, ' bold '),fg='black',yscrollcommand = scrollbar.set )
mylist.insert(tk.END,'You can see execution messages here!!!')
mylist.place(x=400,y=500)
scrollbar.config( command = mylist.yview )

#getting the files from directory
def dir_files():
    files_in_dir=[]
    for r, d, f in os.walk(txt.get()):
        for item in f:
            if '.jpg' in item :
                files_in_dir.append(item)
    return files_in_dir

#defining function if user choses "update one at a time"
def sel():
    R2.deselect() 
    files_in_dir=dir_files()
    print(files_in_dir)
    choices={i for i in files_in_dir}  #getting all the files in the directory
    tkvar.set(' ')
    popupMenu = tk.ttk.OptionMenu(window, tkvar,*choices)   #using to display a option menu
    tk.Label(window, text="Choose the file",bg ="light goldenrod yellow", 
    fg ="black", font = ('calibri', 12, ' bold ')).place(x=425,y=350)  #setting atrributes
    popupMenu.config(width=15)
    popupMenu.place(x=550,y=350)   
    selection = "You selected the option " + str(var.get())
    #inserting appropiate messages
    mylist.insert(tk.END, "You selected the option: Update one at a time")
    label.config(text = selection)

#creating the radio button "Update one at a time"
R1 = tk.Radiobutton(window, text="Update file one at a time",height=1,variable=var, value=1,
              command=sel,fg ="black", bg = "light goldenrod yellow", font = ('calibri', 15, ' bold '))
R1.place(x=400,y=300) #setting atrributes

#defining the function to execute for "Update all at once"
def sel2():
    R1.deselect()
    #inserting appropiate message
    mylist.insert(tk.END, "You selected the option: Update all at once")
    selection = "You selected the option " + str(var.get())
    label.config(text = selection)

#creating the rado button for "Update all at once"
R2 = tk.Radiobutton(window, text="Update all at once",height=1, variable=var, value=2,fg ="black",
                    bg = "light goldenrod yellow",
                    font = ('calibri', 15, ' bold '),command=sel2)
R2.place(x=400,y=400)

#crating the csv file in the directory being used
csv_path=os.getcwd()+'\\details.csv'
exists_csv = os.path.isfile(csv_path)

#creating the file if the csv file does not exists
if not exists_csv:
    head=['SID','FIRST NAME','MIDDLE NAME','LAST NAME','ADDRESS','CITY','DISTRICT','STATE','PIN-CODE','FATHER\'S NAME',
          'FATHER\'S CONTACT','MOTHER\'S NAME','MOTHER\'S CONTACT','DATE OF BIRTH','GENDER','BLOOD GROUP','CONTACT',
          'LANDLINE','EMAIL-ID']
    with open("details.csv","a+") as w:
        c=csv.writer(w)
        c.writerow(head)      #creating the file with head

#creating the text file to keep a track of records being updated
tr_path=os.getcwd()+'\\track.txt'
exists = os.path.isfile(tr_path)
if not exists:
    trackw= open("track.txt","w+")
    trackw.close()
#writing excel
def write(l):
    with open("details.csv","a+") as wb:
        c=csv.writer(wb,lineterminator='\n')
        c.writerow(l)
#build csv format:
def csv_format(lbls_data):
    #csv format is created using the format in the file used
    lbls=[]
    for i in list(lbls_data.keys()):
        if i==0:
            lbls.append(lbls_data[i])
        if i==1:
            lbls.append(lbls_data[i][:30].rstrip())
            lbls.append(lbls_data[1][30:57].rstrip())
            lbls.append(lbls_data[1][57:87].rstrip())
        if i==2:
            lbls.append(" ".join(lbls_data[i][0:140].strip().split()))
            lbls.append(" ".join(lbls_data[i][140:170].strip().split()))
            lbls.append(" ".join(lbls_data[i][170:201].strip().split()))
            lbls.append(" ".join(lbls_data[i][201:232].strip().split()))
            lbls.append(" ".join(lbls_data[i][232:].strip().split()))
        if i==3:
            lbls.append(" ".join(lbls_data[i][:29].strip().split()))
            lbls.append(" ".join(lbls_data[i][29:58].strip().split()))
            lbls.append(" ".join(lbls_data[i][58:87].strip().split()))
            lbls.append(" ".join(lbls_data[i][87:96].strip().split()))
        if i==4:
            lbls.append(" ".join(lbls_data[i][:8].strip().split()))
            lbls.append(" ".join(lbls_data[i][8:20].strip().split()))
            lbls.append(lbls_data[i][20:22]+'-'+lbls_data[i][22:])
        if i==5:
            lbls.append(" ".join(lbls_data[i][:10].strip().split()))
            lbls.append(lbls_data[i][10:15]+'-'+lbls_data[i][15:25])
            lbls.append(" ".join(lbls_data[i][25:].strip().split()))
    write(lbls)

#defing the update function for update button
def update():
    rd=var.get()
    #getting the file name already updated
    with open("track.txt","r+") as trackr:
        data=trackr.read()
    j=data.split()
    if rd==1:
        #updating the file at once
        mylist.insert(tk.END, "You selected file "+str(tkvar.get())) #inserting appropiate message
        if tkvar.get() not in j:
            path=txt.get()+"\\"+tkvar.get()
            flbl = getdata(path) #calling the function to get the predicted labels
            print(flbl)
            #adding the file in track.txt for tracking and also creating the csv file
            if flbl:
                with open("track.txt","a+") as trackw:
                    trackw.write(tkvar.get()+'\n')
                mylist.insert(tk.END,'Updating csv file...')
                csv_format(flbl)
                time.sleep(0.5)
                mylist.insert(tk.END,str(tkvar.get())+' file updated successfully!!')
        else:
            #informing the user that the file has been already updated
            mylist.insert(tk.END, str(tkvar.get())+' file is already updated')
            print(tkvar.get()+' already updated')
    if rd==2:
        #updating all at once
        files_in_dir=dir_files() #calling the function to get the files in the directory
        for i in files_in_dir: #running the loop to update each file with eac iteration
            if i not in j:
                path=txt.get()+"\\"+i
                print(path)
                flbl = getdata(path) #calling the function to get predicted labels
                #adding the file in track.txt for tracking and also creating the csv file
                if flbl:  
                    with open("track.txt","a") as trackw:
                        trackw.write(i+'\n')
                    mylist.insert(tk.END,'Updating csv file...')
                    csv_format(flbl)
                    mylist.insert(tk.END,str(i)+' file updated successfully!!')
            else:
                #informing the user that the file has been already updated
                mylist.insert(tk.END,str(tkvar.get())+' file is already updated')
                print(tkvar.get()+' already updated')
        
#definingg The update button
update = tk.Button(window, text ="Update", 
command = update,
width = 15, height = 1, font =('calibri', 15, ' bold '))  #setting atrributes
update.place(x = 800, y = 400 )

label = tk.Label(window)

window.mainloop()

